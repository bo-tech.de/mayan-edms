��    	      d      �       �      �      �   /   �   "   (     K  &   Z  *   �     �  �  �     �     �  3   �  %   �       #   -  4   Q     �                                       	       Document Document file Document file "%(object)s" queued for download. Document files ready for download. Download files Download files of document: %(object)s Generate a compressed document file bundle Quick download Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-09-06 21:36+0000
Last-Translator: tomkolp, 2023
Language-Team: Polish (https://app.transifex.com/rosarior/teams/13584/pl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pl
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 Dokument Plik dokumentu Plik dokumentu " %(object)s" w kolejce do pobrania. Pliki dokumentów gotowe do pobrania. Pobierz pliki Pobierz pliki dokumentu: %(object)s Wygeneruj pakiet skompresowanych plików dokumentów Szybkie pobieranie 